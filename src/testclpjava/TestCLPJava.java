package testclpjava;

import commandline.OptionManager;
import ilog.concert.IloException;
import java.util.Random;

/**
 * Test the clp-java library by running a random assignment problem using it
 (comparing the results to running the same problem in CPLEXmodel).
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class TestCLPJava {

  /** Constructor. */
  private TestCLPJava() { }

  /**
   * Set up and run the comparison. The command line arguments are the
   * dimension of the problem and a seed for the random number generator.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    int seed = 0;          // random number seed
    int dim = 1;           // problem dimension
    long time;             // time stamp
    String export = null;  // name of CPLEXmodel export file
    OptionManager mgr = new OptionManager("=", false);
    try {
      // set up the command line manager
      mgr.declareKeyvalPairOption("dimension", "dim", Integer.class, 100);
      mgr.declareKeyvalPairOption("seed", "seed", Integer.class, 12345);
      mgr.declareKeyvalPairOption("export", "export", String.class, null);
      // parse the command line.
      mgr.parse(args);
      // get the argument values
      seed = mgr.fetchInt("seed");
      dim = mgr.fetchInt("dimension");
      if (dim <= 0) {
        System.err.println("Invalid dimension: " + dim);
        System.exit(1);
      }
      export = mgr.fetchString("export");
    } catch (OptionManager.OptionManagerException ex) {
      System.err.println("Error parsing command line:\n" + ex.getMessage());
      System.exit(1);
    }
    // set up and seed the random number generator
    Random rng = new Random(seed);
    // generate the cost matrix
    double[][] cost = new double[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        cost[i][j] = rng.nextDouble();
      }
    }
    System.out.println("Test problem dimension = " + dim);
    // set up an LP using clp-java
    time = System.currentTimeMillis();
    CLPmodel clp = new CLPmodel(cost);
    time = System.currentTimeMillis() - time;
      System.out.println("\nCLP model setup time = " + time + " ms.");
    // solve the LP
    time = System.currentTimeMillis();
    double ov= clp.solve();
    time = System.currentTimeMillis() - time;
    if (Double.isNaN(ov)) {
      System.err.println("CLP failed to solve the model; status = "
                         + clp.getStatus());
      System.exit(1);
    } else {
      System.out.println("CLP solution time = " + time
                         + " ms.\nObjective value = " + ov);
    }
    // recover the solution
    time = System.currentTimeMillis();
    double[][] x = clp.getAssignments();
    time = System.currentTimeMillis() - time;
    System.out.println("CLP solution recovered in " + time + " ms.");
    // repeat with CPLEX
    double[][] xx = null;
    try {
      time = System.currentTimeMillis();
      // set up an LP using CPLEXmodel
      CPLEXmodel cplex = new CPLEXmodel(cost);
      time = System.currentTimeMillis() - time;
      System.out.println("\nCPLEX model setup time = " + time + " ms.");
      // solve the LP
      time = System.currentTimeMillis();
      ov = cplex.solve();
      time = System.currentTimeMillis() - time;
      System.out.println("CPLEX solution time = " + time
                         + " ms.\nObjective value = " + ov);
      // recover the solution
      time = System.currentTimeMillis();
      xx = cplex.getAssignments();
      time = System.currentTimeMillis() - time;
      System.out.println("CPLEX solution recovered in " + time + " ms.");
      if (export != null) {
        cplex.exportModel(export);
      }
    } catch (IloException ex) {
      System.err.println("CPLEX blew up:\n" + ex.getMessage());
      System.exit(1);
    }
    // compare the solutions and list discrepancies
    int ndiff = 0;  // # of differences
    System.out.println("\nSolution differences:\n\ti\tj\tCLP\tCPLEX");
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        if (Math.abs(x[i][j] - xx[i][j]) > 0.5) {
          ndiff++;
          System.out.println("\t" + i + "\t" + j
                             + "\t" + x[i][j] + "\t" + xx[i][j]);
        }
      }
    }
    if (ndiff == 0) {
      System.out.println("\nSolutions are the same.");
    } else {
      System.out.println("\nTotal number of differences = " + ndiff);
    }
  }

}
