package testclpjava;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Status;

/**
 * CPLEXmodel is a CPLEX model for the assignment problem.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CPLEXmodel {
  private final int dim;               // problem dimension
  private final IloCplex lp;           // the LP model
  private final IloNumVar[][] assign;  // assign[i][j] = 1 if i is assigned to j
  private Status status;               // solver status

  /**
   * Constructor.
   * @param cost the assignment problem cost matrix (must be square)
   * @throws IloException if the LP model cannot be built
   */
  public CPLEXmodel(final double[][] cost) throws IloException {
    dim = cost.length;
    lp = new IloCplex();
    // create the variables
    assign = new IloNumVar[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        assign[i][j] = lp.numVar(0.0, 1.0, "x_" + i + "_" + j);
      }
    }
    // constraint: every subject must be assigned exactly once
    for (int i = 0; i < dim; i++) {
      lp.addEq(lp.sum(assign[i]), 1.0, "rowsum_" + i);
    }
    // constraint: every slot must be filled exactly once
    for (int j = 0; j < dim; j++) {
      IloLinearNumExpr expr = lp.linearNumExpr();
      for (int i = 0; i < dim; i++) {
        expr.addTerm(1.0, assign[i][j]);
      }
      lp.addEq(expr, 1.0, "colsum_" + j);
    }
    // objective: minimize total assignment cost
    IloLinearNumExpr obj = lp.linearNumExpr();
    for (int i = 0; i < dim; i++) {
      obj.add(lp.scalProd(assign[i], cost[i]));
    }
    lp.addMinimize(obj);
    // limit CPLEX to a single thread (since CLP is single-threaded)
    lp.setParam(IloCplex.Param.Threads, 1);
  }

  /**
   * Solve the model.
   * @return the optimal objective value
   * @throws IloException if solution fails
   */
  public double solve() throws IloException {
    lp.solve();
    status = lp.getStatus();
    if (status == Status.Optimal) {
      return lp.getObjValue();
    } else {
      throw new IloException("No solution found!");
    }
  }

  /**
   * Get the solver status.
   * @return the solver status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Get the optimal assignments.
   * @return the array of assignment decisions
   * @throws IloException if the solution cannot be recovered
   */
  public double[][] getAssignments() throws IloException {
    if (status == Status.Optimal) {
      double[][] sol = new double[dim][];
      for (int i = 0; i < dim; i++) {
        sol[i] = lp.getValues(assign[i]);
      }
      return sol;
    } else {
      throw new IloException("No solution found!");
    }
  }

  /**
   * Export the model.
   * @param file the output file name
   * @throws IloException if export fails
   */
  public void exportModel(final String file) throws IloException {
    lp.exportModel(file);
  }
}
