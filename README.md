This repository has code used in the blog post ["Using CLP with Java"](http://orinanobworld.blogspot.com/2016/06/using-clp-with-java.html).
In addition to clp-java and CPLEX (external dependencies), it uses a command line parser I wrote (available from SourceForge under the project
name [JCLILIB](https://sourceforge.net/projects/jclilib/). It may be easier just to remove references to the "commandline" package in the main
program and either hard code the inputs or handle them your own way.